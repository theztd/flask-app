FROM python:3-slim

LABEL version=""
LABEL authors="Marek Sirovy"
LABEL contact="msirovy@gmail.com"
LABEL description="Demo application for CI/CD demonstration"

# Copy application source to image
COPY ./src/ /opt/flask-app/

# Set application's home as workdir
WORKDIR /opt/flask-app/

# Install deps
RUN pip3 install -r requirements.txt

# Set ENV
ENV PORT=3000
ENV THREAD_COUNT=4

# Expose port
EXPOSE 3000

# Entrypoint
CMD ["/opt/flask-app/entrypoint.sh"]
